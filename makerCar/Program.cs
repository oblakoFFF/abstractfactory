﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Не совсем абстрактная фабрика, но сойдет
// Именна методов и полей, названы понятно, думаю не будет сложностей

namespace MakerCar
{
    class Car
    {
        protected const double m_percentHourcePowerToKH = 3.6;
        protected double nowSpeed;
        protected int maxSpeedKH;
        protected int hourcePower;
        public virtual void getDescription() { }
        public bool setRacing(int speedKH)
        {
            if (speedKH <= maxSpeedKH)
            {
                do
                {
                    if (nowSpeed < speedKH)
                    {
                        nowSpeed += m_percentHourcePowerToKH;
                        if (nowSpeed > speedKH) nowSpeed = speedKH;
                    }
                    else if (nowSpeed > speedKH)
                    {
                        nowSpeed -= m_percentHourcePowerToKH;
                        if (nowSpeed < speedKH) nowSpeed = speedKH;
                    }
                    Console.WriteLine("Скорость: " + nowSpeed);
                } while (nowSpeed != speedKH);
                return true;
            }
            else return false;
        }
    }

    class SedanCar : Car
    {
        private SedanCar() { }
        public SedanCar(int maxSpeedKH, int hourcePower)
        {
            nowSpeed = 150;
            this.maxSpeedKH = maxSpeedKH;
            this.hourcePower = hourcePower;
        }
        public override void getDescription()
        {
            Console.WriteLine("Я седан");
        }
    }

    class SUVCar : Car
    {
        private SUVCar() { }
        public SUVCar(int maxSpeedKH, int hourcePower)
        {
            nowSpeed = 0;
            this.maxSpeedKH = maxSpeedKH;
            this.hourcePower = hourcePower;
        }
        public override void getDescription()
        {
            Console.WriteLine("Я внедорожник");
        }
    }

    class CrossoverCar : Car
    {
        private CrossoverCar() { }
        public CrossoverCar(int maxSpeedKH, int hourcePower)
        {
            nowSpeed = 0;
            this.maxSpeedKH = maxSpeedKH;
            this.hourcePower = hourcePower;
        }
        public override void getDescription()
        {
            Console.WriteLine("Я кроссовер");
        }
    }


    abstract class CarFactory
    {
        public abstract Car CreateCar(int maxSpeedKH, int hourcePower);
    }

    class SedanCreator : CarFactory
    {
        public override Car CreateCar(int maxSpeedKH, int hourcePower)
        {
            return new SedanCar(maxSpeedKH, hourcePower);
        }
    }

    class SUVCreator : CarFactory
    {
        public override Car CreateCar(int maxSpeedKH, int hourcePower)
        {
            return new SUVCar(maxSpeedKH, hourcePower);
        }
    }

    public class Example
    {
        public static void Main(String[] args)
        {
            CarFactory mySedanFactory = new SedanCreator();
            Car mySedan = mySedanFactory.CreateCar(170, 200);
            mySedan.getDescription();
            mySedan.setRacing(120);

            Console.ReadKey();
        }
    }
}
